from telebot.apihelper import ApiTelegramException

import DatabaseHandler
import logging

logger = logging.getLogger("mariano")


class MessageHandler:
    def __init__(self, bot, message, config):
        self.bot = bot
        self.message = message
        self.chat_id = message.chat.id
        self.user_id = message.from_user.id
        self.shared_group = config.shared_group
        if message.text is not None:
            args = message.text.split()
            self.command = args[0]
            self.parameters = args[1:]
        else:
            self.command = ""
            self.parameters = ""
        logger.info("Recibido: chat_id:[%s] comando: [%s] con parámetros: %s", self.chat_id, self.command, self.parameters)
        self.db = DatabaseHandler.DatabaseHandler(config.db_name)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            self.db.commit()
        else:
            logger.error("Error processing command: {} Error: {}", self.command, exc_val)
            self.db.rollback()

        self.db.close()

    def valid_user(self):
        if self.db.get_player_name(self.user_id) is None:
            self.send_private_message("Debe estar registrado en el salon del ADLS")
            return False
        return True

    def in_shared_group(self, send_message=True):
        if self.chat_id != self.shared_group:
            if send_message:
                self.send_private_message("Este comando sólo puede usarse en el canal del ADLS")
            return False
        return True

    def send_private_message(self, text, **kwargs):
        self.send_split_messages(self.user_id, kwargs, text)

    def send_shared_message(self, text, **kwargs):
        self.send_split_messages(self.shared_group, kwargs, text)

    def send_split_messages(self, chat_id, kwargs, text):
        text = text.encode('utf-8')
        if len(text) < 4000:
            self.send_message(chat_id, kwargs, text, 1, 1)
        else:
            parts = split_message(text)
            for number, message in enumerate(parts):
                self.send_message(chat_id, kwargs, message, number + 1, len(parts))

    def send_message(self, chat_id, kwargs, text, number, total):
        redirect_errors = [
            "Forbidden: bot can't initiate conversation with a user",
            "Forbidden: bot was blocked by the user"
        ]
        tries = 0
        send_to_chat = chat_id
        while tries < 5:
            tries += 1
            logger.info("Enviando respuesta. Intento: intento: %d/5", tries)
            try:
                self.bot.send_message(send_to_chat, text, **kwargs)
                tries = 5
            except ApiTelegramException as ex:
                logger.error("Error enviando mensaje: {%s}", ex)
                error_code = ex.result_json["error_code"] if "error_code" in ex.result_json else 0
                description = ex.result_json["description"] if "description" in ex.result_json else ""
                if error_code == 403 and description in redirect_errors:
                    send_to_chat = self.shared_group
            except Exception as ex:
                logger.error("Error enviando mensaje: {%s}", ex)
        logger.info("Respuesta %d/%d: [%s] ", number, total, text)


def split_message(text):
    nl = ord("\n")
    parts = []
    start = 0
    for ix, car in enumerate(text):
        if car == nl:
            part_length = ix - start
            if part_length > 3000:
                parts.append(text[start:ix])
                start = ix+1
    parts.append(text[start:])
    return parts
