import sqlite3
import logging

logger = logging.getLogger("mariano")


class DatabaseHandler:
    def __init__(self, chat_id):
        database_name = f"DB-{chat_id}.db"
        self.connection = sqlite3.connect(database_name)
        self._ensure_schema()

    def _ensure_schema(self):
        cursor = self.connection.cursor()
        version = cursor.execute("PRAGMA user_version").fetchone()[0]
        if version < 1:
            self._apply_schema_1()
        if version < 2:
            self._apply_schema_2()

    def _apply_schema_1(self):
        logger.debug("Updating schema to version 1")
        cursor = self.connection.cursor()
        cursor.execute("create table ProposedGames (id INTEGER primary key autoincrement, userId int, gameName text);")
        cursor.execute("create table Players (id INTEGER primary key autoincrement, userId int, userName text, firstName text, lastName text);")
        cursor.execute("create table Rounds (id INTEGER primary key autoincrement, roundNumber int, gameName text);")
        cursor.execute(
            "create table Credits (id INTEGER primary key autoincrement, userId int, roundId int, used boolean);")
        cursor.execute(
            "create table Scores (id INTEGER primary key autoincrement, userId int, roundId int, score int, date integer);")
        cursor.execute("PRAGMA user_version=1")
        self.connection.commit()

    def _apply_schema_2(self):
        logger.debug("Updating schema to version 2")
        cursor = self.connection.cursor()
        cursor.execute("CREATE TABLE Teams (id INTEGER primary key autoincrement, name text, icon text);")
        cursor.execute("CREATE TABLE TeamMembers (id INTEGER primary key autoincrement, playerId int, teamId int);")
        cursor.execute("PRAGMA user_version=2")
        self.connection.commit()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            self.commit()
        else:
            self.rollback()
        self.close()

    def close(self):
        self.connection.close()

    def rollback(self):
        self.connection.rollback()

    def commit(self):
        self.connection.commit()

    def list_players(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT userName, firstName, LastName FROM Players ORDER BY userName")
        return cursor.fetchall()

    def get_player_name(self, user_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT userName, firstName FROM Players Where userId = ?", (user_id,))
        user = cursor.fetchone()
        return user

    def get_player_id(self, name):
        cursor = self.connection.cursor()
        cursor.execute("SELECT userId FROM Players Where firstName = ?", (name,))
        user = cursor.fetchone()
        return user if user is None else user[0]

    def get_player_id_by_username(self, username):
        cursor = self.connection.cursor()
        cursor.execute("SELECT userId FROM Players Where userName = ?", (username,))
        user = cursor.fetchone()
        return user if user is None else user[0]

    def player_name_exists(self, name, user_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT count(*) FROM Players Where userName = ? and userId != ?", (name, user_id))
        user = cursor.fetchone()
        return user[0] > 0

    def create_player(self, user_id, user_name, first_name, last_name):
        cursor = self.connection.cursor()
        cursor.execute("INSERT INTO Players (userId, userName, firstName, lastName) VALUES(?, ?, ?, ?)", (user_id, user_name, first_name, last_name))

    def update_player_name(self, user_id, user_name):
        cursor = self.connection.cursor()
        cursor.execute("UPDATE Players SET userName = ? WHERE userId = ?", (user_name, user_id))

    def get_games_list(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT gameName FROM Rounds ORDER BY gameName")
        games = cursor.fetchall()
        return list(map(lambda x: x[0], games))

    def get_recent_games_list(self, from_round):
        cursor = self.connection.cursor()
        cursor.execute("SELECT gameName FROM Rounds WHERE roundNumber > ? ORDER BY gameName", (from_round,))
        games = cursor.fetchall()
        return list(map(lambda x: x[0], games))


    def get_game_id(self, game_name):
        cursor = self.connection.cursor()
        cursor.execute("SELECT roundNumber FROM Rounds WHERE gameName = ?", (game_name,))
        game = cursor.fetchone()
        return game if game is None else game[0]

    def get_unused_player_credits(self, user_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT roundId, used FROM Credits WHERE userId = ? and used = 0", (user_id,))
        return cursor.fetchall()

    def get_player_game_credits(self, user_id, game_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT count(*) FROM Credits WHERE userId = ? and roundId = ?", (user_id, game_id))
        return cursor.fetchone()[0]

    def create_player_credit(self, user_id, game_id):
        cursor = self.connection.cursor()
        cursor.execute("INSERT INTO Credits (userId, roundId, used) VALUES (?, ?, 0)", (user_id, game_id))

    def use_player_credit(self, user_id, game_id):
        cursor = self.connection.cursor()
        cursor.execute("UPDATE Credits SET used = 1 WHERE userId = ? and roundId = ?", (user_id, game_id))

    def save_proposed_game(self, user_id, game_name):
        cursor = self.connection.cursor()
        cursor.execute("INSERT INTO ProposedGames (userId, gameName) VALUES (?, ?)",
                       (user_id, game_name))

    def get_proposed_games_votes(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT pg.gameName, count(pg.userId) as votes "
                       "FROM ProposedGames pg "
                       "GROUP BY pg.gameName "
                       "ORDER BY pg.gameName COLLATE NOCASE")
        return cursor.fetchall()

    def get_proposed_games(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT pl.userName, pg.gameName "
                       "FROM ProposedGames pg inner join Players pl on pg.userId == pl.userId "
                       "ORDER BY pg.gameName")
        return cursor.fetchall()

    def update_proposed_game(self, game_id, game_name):
        cursor = self.connection.cursor()
        cursor.execute("UPDATE ProposedGames SET gameName = ? WHERE id = ?", (game_name, game_id))

    def get_proposed_games_of_user(self, user_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT id, pg.gameName FROM ProposedGames pg WHERE pg.userId = ? ORDER BY pg.gameName", (user_id, ))
        return cursor.fetchall()

    def save_score(self, game_id, user_id, score):
        cursor = self.connection.cursor()
        cursor.execute("INSERT INTO Scores (userId, roundId, score, date) VALUES(?, ?, ?, datetime('now'))", (user_id, game_id, score))

    def remove_score(self, game_id, user_id, score_id):
        cursor = self.connection.cursor()
        cursor.execute("DELETE FROM Scores WHERE userId=? AND roundId = ? and id = ?", (user_id, game_id, score_id))

    def get_player_scores(self, user_id, game_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT id, score, date FROM Scores WHERE userId = ? and roundId = ? order by date desc", (user_id, game_id))
        return cursor.fetchall()

    def score_exists(self, game_id, user_id, score):
        cursor = self.connection.cursor()
        cursor.execute("SELECT count(*) FROM Scores WHERE userId = ? and roundId = ? and score = ?", (user_id, game_id, score))
        return cursor.fetchone()[0] > 0

    def get_top(self, game_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT P.userName, max(score) as maxscore, t.icon "
                       "FROM Scores S "
                       "INNER JOIN Players P on S.userId = P.userId "
                       "LEFT OUTER JOIN TeamMembers tm on P.userId = tm.playerId "
                       "LEFT OUTER JOIN Teams t on tm.teamId = t.id "
                       "WHERE roundId = ? "
                       "GROUP BY P.userName "
                       "ORDER BY maxscore DESC", (game_id,))
        return cursor.fetchall()

    def remove_proposed_games(self, game_name):
        cursor = self.connection.cursor()
        cursor.execute("DELETE FROM ProposedGames WHERE gameName == ?", (game_name, ))

    def get_last_round_number(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT max(roundNumber) FROM Rounds ")
        round_number = cursor.fetchone()[0]
        return 0 if round_number is None else round_number

    def create_round(self, number, game):
        cursor = self.connection.cursor()
        cursor.execute("INSERT INTO Rounds (roundNumber, gameName) VALUES(?, ?)", (number, game))

    def get_round(self, number):
        cursor = self.connection.cursor()
        cursor.execute("SELECT roundNumber, gameName FROM Rounds WHERE roundNumber == ?", (number, ))
        return cursor.fetchone()

    def list_previous_rounds(self, actual):
        cursor = self.connection.cursor()
        cursor.execute("SELECT roundNumber, gameName FROM Rounds WHERE roundNumber < ?", (actual, ))
        return cursor.fetchall()

    def get_team_by_name(self, team_name):
        cursor = self.connection.cursor()
        cursor.execute("SELECT id, name, icon FROM Teams WHERE name = ?", [team_name])
        return cursor.fetchone()

    def get_team_count(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT count(name) FROM Teams", [])
        return cursor.fetchone()[0]

    def add_team(self, team_name, icon):
        cursor = self.connection.cursor()
        cursor.execute("INSERT INTO Teams (name, icon) VALUES(?, ?)", [team_name, icon])

    def get_team_player(self, team_id, player_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT id, playerId, teamId FROM TeamMembers WHERE playerId = ? AND teamId = ?", [player_id, team_id])
        return cursor.fetchone()

    def add_team_member(self, team_id, player_id):
        cursor = self.connection.cursor()
        cursor.execute("INSERT INTO TeamMembers (playerId, teamId) VALUES(?, ?)", [player_id, team_id])

    def remove_team_member(self, team_id, player_id):
        cursor = self.connection.cursor()
        cursor.execute("DELETE FROM TeamMembers WHERE playerId=? AND teamId=?", [player_id, team_id])


    def get_existing_team_members(self, team_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT t.id, t.playerId, p.userName FROM TeamMembers t INNER JOIN main.Players p on p.userId = t.playerId WHERE teamId = ?", [team_id, ])
        return cursor.fetchall()
