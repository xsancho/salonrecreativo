#!/bin/bash

YEAR=`date +"%Y"`
MONTH=`date +"%m"`
DAY=`date +"%d"`

FOLDER=/home/federico/Proyectos/salonrecreativo/
DB=DB--1001371988682.db
BACKUP_SUFFIX=$YEAR$MONTH$DAY

cp $FOLDER/$DB backup/$DB.$BACKUP_SUFFIX
cp $FOLDER/MessageHandler.py backup/MessageHandler.py.$BACKUP_SUFFIX
cp $FOLDER/DatabaseHandler.py backup/DatabaseHandler.py.$BACKUP_SUFFIX
cp $FOLDER/Mariano.py backup/Mariano.py.$BACKUP_SUFFIX

